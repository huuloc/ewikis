from django import template
register = template.Library()
from django.db.models import Q
@register.simple_tag
def active(request, pattern):
    import re
    if re.search(pattern, request.path):
        return 'active'
    return ''
