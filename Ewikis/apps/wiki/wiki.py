from pure_pagination.paginator import Paginator
from django.shortcuts import get_object_or_404
from django.http import Http404
from django.db.models import Q

def get_pre_or_next_section(current_section, section):
	pre = None
	next = None
	found = False
	for sec in section:
		if found:
			next = sec
			break
		if sec.id == current_section.id:
			found = True
			continue
		pre = sec
	return (pre, next)
def swap_data(sec1, sec2):
	title = sec1.title
	content = sec1.content
	content_markdown = sec1.content_markdown
	sec1.title = sec2.title
	sec1.content  = sec2.content
	sec1.content_markdown = sec2.content_markdown
	sec1.save()
	sec2.title = title
	sec2.content = content
	sec2.content_markdown = content_markdown
	sec2.save()
def filterWiki(request,subject,Wiki):
	sub_Subject ={'MATH','LANGUAGE', 'SCIENCE', 'HISTORY'}
	subject = subject.upper()
	if subject == 'ALL':
		wiki =  Paginator(Wiki.objects.all().order_by('-like'),5,request=request)
		return wiki.page(request.GET.get('page',1))
	elif subject in sub_Subject:
		if subject == "MATH":
			wiki = Paginator(Wiki.objects.filter(
				Q(subject='ALEGEBRA')|
				Q(subject='ARITHMETIC')|
				Q(subject='CALCULUS')|
				Q(subject='GEOMETRY')|
				Q(subject='STATISTICS')
				).order_by('-like')[:5],5,request=request)
			return wiki.page(request.GET.get('page',1))
		if subject == "HISTORY":
			wiki = Paginator(Wiki.objects.filter(
			Q(subject='CHINA')|
			Q(subject='VIETNAM')|
			Q(subject='WORLD')
			).order_by('-like')[:5],5,request=request)
			return wiki.page(request.GET.get('page',1))
		if subject == "LANGUAGE":
			wiki = Paginator(Wiki.objects.filter(
			Q(subject='CHINESE')|
			Q(subject='VIETNAMESE')|
			Q(subject='ENGLISH')|
			Q(subject='SPANISH')
			).order_by('-like')[:5],5,request=request)
			return wiki.page(request.GET.get('page',1))
		if subject == "SCIENCE":
			wiki = Paginator(Wiki.objects.filter(
			Q(subject='BIOLOGY')|
			Q(subject='CHEMISTRY')|
			Q(subject='EARTHSICENCE')|
			Q(subject='PHYSICS')|
			Q(subject='PSYCHOLOGY')|
			Q(subject='MEDICINE')
			).order_by('-like')[:5],5,request=request)
			return wiki.page(request.GET.get('page',1))
	else:
		wiki =  Paginator(Wiki.objects.filter(subject = subject).order_by('-like'),5,request=request)
		return wiki.page(request.GET.get('page',1))



