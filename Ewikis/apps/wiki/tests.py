#encoding:UTF-8

from selenium import selenium
import unittest, time, re

class test(unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium("localhost", 4444, "*chrome", "http://wikis.herokuapp.com/")
        self.selenium.start()
    
    def test_a(self):
        sel = self.selenium
        sel.open("/")
        sel.click("link=Login")
        sel.wait_for_page_to_load("30000")
        sel.type("id=id_username", "clone17")
        sel.type("id=id_password", "clone1717")
        sel.click("css=input.btn.btn-success")
        sel.wait_for_page_to_load("30000")
    
    def test_youtube(self):
        sel = self.selenium
        sel.open("/ubuntu/")
        sel.click("css=i..icon-pencil")
        sel.type("id=id_content", "[http://www.youtube.com/watch?v=D6z6hn6wZlg]")
        sel.type("document.forms['Add text'].elements['title']", "Ubuntu new feature")
        sel.click("css=input.btn")
        sel.wait_for_page_to_load("30000")
    
    def test_addwiki(self):
        sel = self.selenium
        sel.open("/ubuntu/addtext/")
        sel.type("id=id_title", "Download Ubuntu")
        sel.type("id=id_content", u"![](http://www.ubuntu.com/sites/www.ubuntu.com/files/active/02_ubuntu/download-picto-desktop-large.png)\n\n## Ubuntu Desktop\n\nOur long-term support (LTS) releases are supported for five years – ideal for organisations undertaking large deployments. To install Ubuntu, you’ll need to create an installation CD or USB stick once your download is complete.\n\n[Read the installation instructions ›](http://www.ubuntu.com/download/help/install-ubuntu-desktop)")
        sel.click("css=input.btn")
        sel.wait_for_page_to_load("30000")
    
    def test_editwiki(self):
        sel = self.selenium
        sel.open("/ubuntu/50/edit/")
        sel.type("id=id_content", "[{slide}http://i.imgur.com/DCNhq.jpg|http://i.imgur.com/aMlL4.jpg|http://i.imgur.com/TUVnF.jpg|http://i.imgur.com/LRJzb.jpg|http://ubuntu.ecchi.ca/wallpapers/9.10//2.jpg|http://i.imgur.com/6rpJB.jpg|http://i.imgur.com/g130C.jpg|http://i.imgur.com/2cy9I.png|http://i.imgur.com/USSzj.jpg|http://i.imgur.com/EUeXq.png|http://ubuntu.ecchi.ca/wallpapers/6.06//1.png|http://i.imgur.com/4v7Mx.jpg|http://i.imgur.com/0gG33.jpg|http://ubuntu.ecchi.ca/wallpapers/4.10//1.png]\n\n![The most beautiful wallpaper of Ubuntu](http://i.imgur.com/2cy9I.png)\nThe most beautiful wallpaper")
        sel.type("id=id_title", "Ubuntu 12.04 wallpaper")
        sel.click("css=input.btn")
        sel.wait_for_page_to_load("30000")
    
    def tearDown(self):
        self.selenium.stop()
        self.assertEqual([], self.verificationErrors)
