from django import forms
from django.forms import ModelForm

from apps.wiki.models import *

class TextForm(ModelForm):
	class Meta:
		model = Section
		fields =('title', 'content')
		widgets = {
			'content': forms.Textarea(attrs = {'cols':10, 'rows': 10}),
			}
	def __init__(self, *args, **kwargs):
		super(TextForm, self).__init__(*args, **kwargs)
		self.fields['title'].widget.attrs['class'] = 'span6'
		self.fields['content'].widget.attrs['class'] = 'span10'
class CreateWikiForm(ModelForm):
	class Meta:
		model = Wiki
		fields = ('title','description', 'gradelevel','subject','lessontype')
		widgets = {
			'description': forms.Textarea(attrs = {'cols':40, 'rows': 6,'style':'width:400px','placeholder':'Description for yours wiki'}),
			}

	def clean_description(self):
		des = self.cleaned_data['description']
		if len(des) < 50:
			raise forms.ValidationError('Your description is smaller than 50 characters')
		return des
	def clean_title(self):
		title = self.cleaned_data['title']
		if len(title) < 3:
			raise forms.ValidationError('Your title is smaller than 3 characters')
		return title
	def __init__(self, *args, **kwargs):
		super(CreateWikiForm, self).__init__(*args, **kwargs)
		self.fields['title'].widget.attrs['style'] = 'width:400px; font-weight:bold; font-size:16px'
		self.fields['title'].error_messages['required'] = 'Type yours title'
		self.fields['description'].error_messages['required'] = 'Make some descriptions for yours wiki'
		self.fields['gradelevel'].error_messages['required'] = 'Choice yours Grade Level'
		self.fields['subject'].error_messages['required'] = 'Choice Subject for wiki'
		self.fields['lessontype'].error_messages['required'] = 'What is the lesson type for your wiki?'

class ImageForm(ModelForm):
	class Meta:
		model = Image
		fields = ('title','photo')
	def clean_photo(self):
		photo = self.cleaned_data['photo']
		if photo.size > 2 * 2 ** 20:
			raise forms.ValidationError('Size photo too big, the maximun size is 2MB')
		return photo
	def __init__(self, *args, **kwargs):
		super(ImageForm, self).__init__(*args, **kwargs)
		self.fields['title'].error_messages['required'] = 'Type yours title'
		self.fields['photo'].error_messages['required'] = 'Please upload a photo' 
   