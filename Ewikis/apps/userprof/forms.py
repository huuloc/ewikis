import re
from django import forms
from django.contrib.auth.models import User

class change_password_form(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput(attrs={"placeholder":"Current password"}), label="Current password")
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={"placeholder":"New password"}), label="New password")
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={"placeholder":"New password (re-type)"}), label="New password (confirm)")

    def clean_password2(self):
        if 'password1' in self.cleaned_data:
            pwd1 = self.cleaned_data['password1']
            pwd2 = self.cleaned_data['password2']
            if len(pwd1) < 6 or len(pwd2) < 6:
                raise forms.ValidationError('Your new password is too short!')
            if pwd1 == pwd2:
                return pwd2
            raise forms.ValidationError("New password does not match~!")
        else:
            raise forms.ValidationError("Smth wrong with your submission")

class change_email_form(forms.Form):
    current_email = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"your_current_email@yahoo.com"}))
    new_email = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"your.new.email@gmail.com"}))
    def clean_email2(self):
        if 'current_email' in self.cleaned_data:
            em1 = self.cleaned_data['current_email']
            em2 = self.cleaned_data['new_email']
            if em1 != em2:
                return em2
            raise forms.ValidationError("Nothing change!")
        else:
            raise forms.ValidationError("Smth wrong with your submission")


class edit_profile_form(forms.Form):
    first_name = forms.CharField(required=False)
    last_name = forms.CharField(required=False)
    website_blog = forms.CharField(label="Website/blog", required=False,
        widget=forms.TextInput(attrs={"placeholder": "http://"}))
    location = forms.CharField(required=False,
        widget=forms.TextInput(attrs={"placeholder":"Hanoi, Vietnam"})
    )
    about = forms.CharField(required=False,
        widget=forms.Textarea(attrs={"placeholder":"Something about you, or your signature, or your favourite quote..."})
    )
    def __init__(self, *args, **kwargs):
        super(edit_profile_form, self).__init__(*args, **kwargs)
        self.fields['about'].widget.attrs['class'] = 'span6'

class forgot_password_form(forms.Form):
    username=forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Your username"}))
    email=forms.EmailField(widget=forms.TextInput(attrs={"placeholder":"Your email"}))


class register_form(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"First name"}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Last name"}))
    username = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"username"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={"placeholder":"Password"}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={"placeholder":"Re-type password"}), label="Re-type password")
    email = forms.EmailField(widget=forms.TextInput(attrs={"placeholder":"Email"}))

    def clean_username(self):
        username = self.cleaned_data['username']
        if not re.search(r'^\w+$', username):
            raise forms.ValidationError('Username can only contain alphanumeric characters and the underscore.')
        try:
            User.objects.get(username=username)
        except:
            return username
        raise forms.ValidationError('Username is not available!')

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            User.objects.get(email=email)
        except:
            return email
        raise forms.ValidationError('Email is not available!')

    def clean_password2(self):
        if 'password' in self.cleaned_data:
            pwd = self.cleaned_data['password']
            pwd2 = self.cleaned_data['password2']
            if len(pwd) < 6 or len(pwd2) < 6:
                raise forms.ValidationError('Your password is too short!')
            if pwd == pwd2:
                return pwd2
            else:
                raise forms.ValidationError('Password does not match!')
        else:
            raise forms.ValidationError("Something wrong with your submission :(")

