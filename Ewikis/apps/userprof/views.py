from pure_pagination.paginator import Paginator


from django.contrib.auth.decorators import login_required
from django.views.generic.simple import direct_to_template
from apps.userprof.forms import *
from django.template.context import RequestContext
from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib.auth import *
from django.contrib.auth.models import check_password, User
from apps.userprof.forms import register_form, change_password_form, edit_profile_form
from apps.userprof.models import Profile
from django.core.mail import send_mail


@login_required
def change_password_page(request):
    profile = get_object_or_404(Profile, user=request.user)
    if request.method == "POST":
        form = change_password_form(request.POST)
        if form.is_valid():
            user= User.objects.get(username=request.user.username)
            raw_password = form.cleaned_data['password']
            new_password = form.cleaned_data['password1']
            enc_password = user.password
            if check_password(raw_password, enc_password):
                user.set_password(new_password)
                user.save()
                send_mail('[Ewikis] Your password is changed!', 'You just changed your password on Ewiki.', 'from@example.com',
                    [user.email], fail_silently=False)
                return direct_to_template(request, "registration/change_password_success.html")
            else:
                return HttpResponseRedirect("/user/change_password")
    else:
        form = change_password_form()
    variables = RequestContext(request, {'form': form, 'profile': profile})
    return render_to_response('registration/change_password.html', variables)


@login_required
def change_email_page(request):
    profile = get_object_or_404(Profile, user=request.user)
    if request.method == "POST":
        form = change_email_form(request.POST)
        if form.is_valid():
            try:
                current_email = form.cleaned_data['email']
                user=User.objects.get(email=current_email, username=request.user.username)
                new_email = form.cleaned_data['email2']
                if new_email != current_email:
                    user.email=new_email
                    user.save()
                    send_mail('[Ewikis] Your email is changed!', 'From now on, your Ewikis account will be connected to email : '+new_email+', instead of '+current_email, 'from@example.com',
                        [current_email, new_email], fail_silently=False)
                    return direct_to_template(request, "registration/change_email_success.html")
            except User.DoesNotExist:
                raise form.errors("Current email is not correct~!")
    else:
        form = change_email_form()
    variables = RequestContext(request, {'form': form,'profile': profile})
    return render_to_response('registration/change_email.html', variables)


@login_required
def edit_profile_page(request):
    profile = get_object_or_404(Profile, user=request.user)
    if request.method == "POST":
        form = edit_profile_form(request.POST)
        if form.is_valid():
            _edit_profile_page(request, form)
            return direct_to_template(request, "registration/edit_profile_success.html")
    else:
        profile = Profile.objects.get(user=request.user)
        first_name = profile.user.first_name
        last_name = profile.user.last_name
        website_blog = profile.website_blog
        location = profile.location
        about = profile.about
        form = edit_profile_form({
            'first_name': first_name,
            'last_name': last_name,
            'website_blog': website_blog,
            'location': location,
            'about': about
        })
    variables = RequestContext(request, {'form': form,'profile': profile})
    return render_to_response('registration/edit_profile.html', variables)


def _edit_profile_page(request, form):
    user, f = User.objects.get_or_create(username=request.user.username)
    user.first_name = form.cleaned_data['first_name']
    user.last_name = form.cleaned_data['last_name']
    user.save()
    profile, f = Profile.objects.get_or_create(user=request.user)
    profile.website_blog = form.cleaned_data['website_blog']
    profile.location = form.cleaned_data['location']
    profile.about = form.cleaned_data['about']
    profile.save()
    return profile


def forgot_password_page(request):
    if request.method == "POST":
        form = forgot_password_form(request.POST)
        if form.is_valid():
            username2Reset=form.cleaned_data['username']
            email2Reset=form.cleaned_data['email']
            try:
                user=User.objects.get(username=username2Reset, email=email2Reset)
                new_password = User.objects.make_random_password(length=10, allowed_chars='1234567890')
                send_mail('[Ewikis] Your new password!', 'Here is your new password: '+new_password, 'from@example.com',
                    [email2Reset], fail_silently=False)
                user.set_password(new_password)
                user.save()
                return direct_to_template(request, "registration/new_password_is_sent.html")
            except User.DoesNotExist:
                raise forms.ValidationError("Username and email do not match any user! or smth wrong occurs")
    else:
        form = forgot_password_form()
    variables = RequestContext(request, {'form': form})
    return render_to_response('registration/forgot_password.html', variables)


def home_page(request):
    try:
        profile = get_object_or_404(Profile, user=request.user)
        interests_page =  Paginator(profile.interest.all().order_by('-like'),5,request=request)
        interests = interests_page.page(request.GET.get('page',1))
        variables = RequestContext(request, {'profile': profile, 'interests':interests})
    except:
        variables = RequestContext(request, {'user': request.user})
    return render_to_response('home.html', variables)


@login_required
def logout_page(request):
    logout(request)
    return HttpResponseRedirect('/wiki/main/')


def register_page(request):
    if request.method == "POST":
        form = register_form(request.POST)
        if form.is_valid():
            user = User.objects.create_user(username=form.cleaned_data['username'],
                password=form.cleaned_data['password2'],
                email=form.cleaned_data['email'])
            user.first_name = form.cleaned_data['first_name']
            user.last_name = form.cleaned_data['last_name']
            user.save()
            prof = Profile.objects.create(user=user, website_blog='', location='', about='')
            prof.save()
            send_mail('[Ewikis] Welcome to Ewikis!', 'Thank you for choosing our services.', 'from@example.com',
                [user.email], fail_silently=False)
            return direct_to_template(request, 'registration/register_success.html')
    else:
        form = register_form()
    variables = RequestContext(request, {'form': form})
    return render_to_response('registration/register.html', variables)


@login_required
def user_profile_page(request, username):
    vuser=get_object_or_404(User, username=username)
    profile=get_object_or_404(Profile, user=vuser)
    interests_page =  Paginator(profile.interest.all().order_by('-like'),5,request=request)
    interests = interests_page.page(request.GET.get('page',1))
    cuser=request.user
    is_owner=False
    if vuser == cuser:
        is_owner=True
    variables=RequestContext(request, {'profile':profile, 'is_owner':is_owner, 'interests':interests})
    return render_to_response('user_profile.html', variables)


@login_required
def add_interest(request, wiki_title):
    p=Profile.objects.get(user=request.user)
    p.interest.add(wiki_title)
    p.save()
    return HttpResponseRedirect('/'+wiki_title)


@login_required
def view_interest(request):
    p=Profile.objects.get(user=request.user)
    interests=p.interest.all()
    variables=RequestContext(request, {'interests':interests})
    return render_to_response('interest.html', variables)