from selenium import selenium
import unittest, time, re

class test172 test 172(unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium("localhost", 4444, "*chrome", "http://wikis.herokuapp.com/")
        self.selenium.start()
    
    def test_test172 test 172(self):
        sel = self.selenium
        sel.open("/")
        sel.click("link=Join now!")
        sel.wait_for_page_to_load("30000")
        sel.type("id=id_first_name", "Test")
        sel.type("id=id_last_name", "Regis")
        sel.type("id=id_username", "test172")
        sel.type("id=id_password", "test172")
        sel.type("id=id_password2", "test172")
        sel.type("id=id_email", "test172@gmail.com")
        sel.click("css=input.btn.btn-info")
        sel.wait_for_page_to_load("30000")
        sel.click("link=Test Regis")
        sel.wait_for_page_to_load("30000")
        sel.click("link=Edit profile")
        sel.wait_for_page_to_load("30000")
        sel.type("id=id_website_blog", "http://wikis.herokuapp.com")
        sel.type("id=id_location", "Hanoi")
        sel.type("id=id_username", "test172")
        sel.type("id=id_password", "test172")
        sel.click("css=input.btn.btn-info")
        sel.wait_for_page_to_load("30000")
        sel.click("link=Wikis")
        sel.wait_for_page_to_load("30000")
        sel.click("link=Test Regis")
        sel.wait_for_page_to_load("30000")
        sel.click("link=Change password")
        sel.wait_for_page_to_load("30000")
        sel.type("id=id_password", "test172")
        sel.type("id=id_password1", "test")
        sel.type("id=id_password2", "test")
        sel.click("css=input.btn.btn-danger")
        sel.wait_for_page_to_load("30000")
        sel.type("id=id_password", "test172")
        sel.type("id=id_password1", "test17172")
        sel.type("id=id_password2", "test172172")
        sel.click("css=input.btn.btn-danger")
        sel.wait_for_page_to_load("30000")
        sel.click("css=input.btn.btn-success")
        sel.wait_for_page_to_load("30000")
    
    def tearDown(self):
        self.selenium.stop()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
