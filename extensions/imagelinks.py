"""
========================= IMAGE LINKS =================================


Turns paragraphs like

<~~~~~~~~~~~~~~~~~~~~~~~~
dir/subdir
dir/subdir
dir/subdir
~~~~~~~~~~~~~~
dir/subdir
dir/subdir
dir/subdir
~~~~~~~~~~~~~~~~~~~>

Into mini-photo galleries.

"""

import re, markdown
import url_manager


IMAGE_LINK = """<a href="%s"><img src="%s" title="%s"/></a>"""
SLIDESHOW_LINK = """<a href="%s" target="_blank">[slideshow]</a>"""
ALBUM_LINK = """&nbsp;<a href="%s">[%s]</a>"""


class ImageLinksExtension(markdown.Extension):
    def __init__(self, configs):
        self.config = {
            'vimeo_width': ['640', 'Width for Vimeo videos'],
        }
    def extendMarkdown(self, md, md_globals):
        md.preprocessors.add("imagelink", ImageLinkPreprocessor(md), "_begin")

class ImageLinkPreprocessor(markdown.preprocessors.Preprocessor):

    def run(self, lines):
        obj = markdown.util.etree.Element('iframe')        
        for line in lines:
            if line.startswith("<~~~~~~~"):
                obj.set('data-auto-height', 'true')
        return obj


def makeExtension(configs=None):
    return ImageLinksExtension(configs=configs)

if __name__ == "__main__":
    import doctest
    doctest.testmod()