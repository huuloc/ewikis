#!/usr/bin/env python

"""
Embeds web videos using URLs.  For instance, if a URL to an youtube video is
found in the text submitted to markdown and it isn't enclosed in parenthesis
like a normal link in markdown, then the URL will be swapped with a embedded
youtube video.

All resulting HTML is XHTML Strict compatible.

>>> import markdown

Test Metacafe

>>> s = "http://www.metacafe.com/watch/yt-tZMsrrQCnx8/pycon_2008_django_sprint_room/"
>>> markdown.markdown(s, ['video'])
u'<p><object data="http://www.metacafe.com/fplayer/yt-tZMsrrQCnx8/pycon_2008_django_sprint_room.swf" height="423" type="application/x-shockwave-flash" width="498"><param name="movie" value="http://www.metacafe.com/fplayer/yt-tZMsrrQCnx8/pycon_2008_django_sprint_room.swf" /><param name="allowFullScreen" value="true" /></object></p>'


Test Metacafe with arguments

>>> markdown.markdown(s, ['video(metacafe_width=500,metacafe_height=425)'])
u'<p><object data="http://www.metacafe.com/fplayer/yt-tZMsrrQCnx8/pycon_2008_django_sprint_room.swf" height="425" type="application/x-shockwave-flash" width="500"><param name="movie" value="http://www.metacafe.com/fplayer/yt-tZMsrrQCnx8/pycon_2008_django_sprint_room.swf" /><param name="allowFullScreen" value="true" /></object></p>'


Test Link To Metacafe

>>> s = "[Metacafe link](http://www.metacafe.com/watch/yt-tZMsrrQCnx8/pycon_2008_django_sprint_room/)"
>>> markdown.markdown(s, ['video'])
u'<p><a href="http://www.metacafe.com/watch/yt-tZMsrrQCnx8/pycon_2008_django_sprint_room/">Metacafe link</a></p>'


Test Markdown Escaping

>>> s = "\\http://www.metacafe.com/watch/yt-tZMsrrQCnx8/pycon_2008_django_sprint_room/"
>>> markdown.markdown(s, ['video'])
u'<p>http://www.metacafe.com/watch/yt-tZMsrrQCnx8/pycon_2008_django_sprint_room/</p>'
>>> s = "`http://www.metacafe.com/watch/yt-tZMsrrQCnx8/pycon_2008_django_sprint_room/`"
>>> markdown.markdown(s, ['video'])
u'<p><code>http://www.metacafe.com/watch/yt-tZMsrrQCnx8/pycon_2008_django_sprint_room/</code></p>'


Test Youtube

>>> s = "http://www.youtube.com/watch?v=u1mA-0w8XPo&hd=1&fs=1&feature=PlayList&p=34C6046F7FEACFD3&playnext=1&playnext_from=PL&index=1"
>>> markdown.markdown(s, ['video'])
u'<p><object data="http://www.youtube.com/v/u1mA-0w8XPo&amp;hd=1&amp;fs=1&amp;feature=PlayList&amp;p=34C6046F7FEACFD3&amp;playnext=1&amp;playnext_from=PL&amp;index=1" height="344" type="application/x-shockwave-flash" width="425"><param name="movie" value="http://www.youtube.com/v/u1mA-0w8XPo&amp;hd=1&amp;fs=1&amp;feature=PlayList&amp;p=34C6046F7FEACFD3&amp;playnext=1&amp;playnext_from=PL&amp;index=1" /><param name="allowFullScreen" value="true" /></object></p>'


Test Youtube with argument

>>> markdown.markdown(s, ['video(youtube_width=200,youtube_height=100)'])
u'<p><object data="http://www.youtube.com/v/u1mA-0w8XPo&amp;hd=1&amp;fs=1&amp;feature=PlayList&amp;p=34C6046F7FEACFD3&amp;playnext=1&amp;playnext_from=PL&amp;index=1" height="100" type="application/x-shockwave-flash" width="200"><param name="movie" value="http://www.youtube.com/v/u1mA-0w8XPo&amp;hd=1&amp;fs=1&amp;feature=PlayList&amp;p=34C6046F7FEACFD3&amp;playnext=1&amp;playnext_from=PL&amp;index=1" /><param name="allowFullScreen" value="true" /></object></p>'


Test Youtube Link

>>> s = "[Youtube link](http://www.youtube.com/watch?v=u1mA-0w8XPo&feature=PlayList&p=34C6046F7FEACFD3&playnext=1&playnext_from=PL&index=1)"
>>> markdown.markdown(s, ['video'])
u'<p><a href="http://www.youtube.com/watch?v=u1mA-0w8XPo&amp;feature=PlayList&amp;p=34C6046F7FEACFD3&amp;playnext=1&amp;playnext_from=PL&amp;index=1">Youtube link</a></p>'


Test Dailymotion

>>> s = "http://www.dailymotion.com/relevance/search/ut2004/video/x3kv65_ut2004-ownage_videogames"
>>> markdown.markdown(s, ['video'])
u'<p><object data="http://www.dailymotion.com/swf/x3kv65_ut2004-ownage_videogames" height="405" type="application/x-shockwave-flash" width="480"><param name="movie" value="http://www.dailymotion.com/swf/x3kv65_ut2004-ownage_videogames" /><param name="allowFullScreen" value="true" /></object></p>'


Test Dailymotion again (Dailymotion and their crazy URLs)

>>> s = "http://www.dailymotion.com/us/video/x8qak3_iron-man-vs-bruce-lee_fun"
>>> markdown.markdown(s, ['video'])
u'<p><object data="http://www.dailymotion.com/swf/x8qak3_iron-man-vs-bruce-lee_fun" height="405" type="application/x-shockwave-flash" width="480"><param name="movie" value="http://www.dailymotion.com/swf/x8qak3_iron-man-vs-bruce-lee_fun" /><param name="allowFullScreen" value="true" /></object></p>'


Test Yahoo! Video

>>> s = "http://video.yahoo.com/watch/1981791/4769603"
>>> markdown.markdown(s, ['video'])
u'<p><object data="http://d.yimg.com/static.video.yahoo.com/yep/YV_YEP.swf?ver=2.2.40" height="322" type="application/x-shockwave-flash" width="512"><param name="movie" value="http://d.yimg.com/static.video.yahoo.com/yep/YV_YEP.swf?ver=2.2.40" /><param name="allowFullScreen" value="true" /><param name="flashVars" value="id=4769603&amp;vid=1981791" /></object></p>'


Test Veoh Video

>>> s = "http://www.veoh.com/search/videos/q/mario#watch%3De129555XxCZanYD"
>>> markdown.markdown(s, ['video'])
u'<p><object data="http://www.veoh.com/videodetails2.swf?permalinkId=e129555XxCZanYD" height="341" type="application/x-shockwave-flash" width="410"><param name="movie" value="http://www.veoh.com/videodetails2.swf?permalinkId=e129555XxCZanYD" /><param name="allowFullScreen" value="true" /></object></p>'


Test Veoh Video Again (More fun URLs)

>>> s = "http://www.veoh.com/group/BigCatRescuers#watch%3Dv16771056hFtSBYEr"
>>> markdown.markdown(s, ['video'])
u'<p><object data="http://www.veoh.com/videodetails2.swf?permalinkId=v16771056hFtSBYEr" height="341" type="application/x-shockwave-flash" width="410"><param name="movie" value="http://www.veoh.com/videodetails2.swf?permalinkId=v16771056hFtSBYEr" /><param name="allowFullScreen" value="true" /></object></p>'


Test Veoh Video Yet Again (Even more fun URLs)

>>> s = "http://www.veoh.com/browse/videos/category/anime/watch/v181645607JyXPWcQ"
>>> markdown.markdown(s, ['video'])
u'<p><object data="http://www.veoh.com/videodetails2.swf?permalinkId=v181645607JyXPWcQ" height="341" type="application/x-shockwave-flash" width="410"><param name="movie" value="http://www.veoh.com/videodetails2.swf?permalinkId=v181645607JyXPWcQ" /><param name="allowFullScreen" value="true" /></object></p>'


Test Vimeo Video

>>> s = "http://www.vimeo.com/1496152"
>>> markdown.markdown(s, ['video'])
u'<p><object data="http://vimeo.com/moogaloop.swf?clip_id=1496152&amp;amp;server=vimeo.com" height="321" type="application/x-shockwave-flash" width="400"><param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id=1496152&amp;amp;server=vimeo.com" /><param name="allowFullScreen" value="true" /></object></p>'

Test Vimeo Video with some GET values

>>> s = "http://vimeo.com/1496152?test=test"
>>> markdown.markdown(s, ['video'])
u'<p><object data="http://vimeo.com/moogaloop.swf?clip_id=1496152&amp;amp;server=vimeo.com" height="321" type="application/x-shockwave-flash" width="400"><param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id=1496152&amp;amp;server=vimeo.com" /><param name="allowFullScreen" value="true" /></object></p>'

Test Blip.tv

>>> s = "http://blip.tv/file/get/Pycon-PlenarySprintIntro563.flv"
>>> markdown.markdown(s, ['video'])
u'<p><object data="http://blip.tv/scripts/flash/showplayer.swf?file=http://blip.tv/file/get/Pycon-PlenarySprintIntro563.flv" height="300" type="application/x-shockwave-flash" width="480"><param name="movie" value="http://blip.tv/scripts/flash/showplayer.swf?file=http://blip.tv/file/get/Pycon-PlenarySprintIntro563.flv" /><param name="allowFullScreen" value="true" /></object></p>'

Test Gametrailers

>>> s = "http://www.gametrailers.com/video/console-comparison-borderlands/58079"
>>> markdown.markdown(s, ['video'])
u'<p><object data="http://www.gametrailers.com/remote_wrap.php?mid=58079" height="392" type="application/x-shockwave-flash" width="480"><param name="movie" value="http://www.gametrailers.com/remote_wrap.php?mid=58079" /><param name="allowFullScreen" value="true" /></object></p>'
"""

import markdown
import random
import re

version = "0.1.6"

class VideoExtension(markdown.Extension):
    def __init__(self, configs):
        self.config = {
            'vimeo_width': ['640', 'Width for Vimeo videos'],
            'vimeo_height': ['360', 'Height for Vimeo videos'],
            'youtube_width': ['80%', 'Width for Youtube videos'],
            'youtube_height': ['360', 'Height for Youtube videos'],
            'scribd_width': ['80%', 'Width for Scribd Doc'],
            'scribd_height': ['600', 'Height for Scribd Doc'],
        }

        # Override defaults with user settings
        for key, value in configs:
            self.setConfig(key, value)

    def add_inline(self, md, name, klass, re):
        pattern = klass(re)
        pattern.md = md
        pattern.ext = self
        md.inlinePatterns.add(name, pattern, "<reference")

    def extendMarkdown(self, md, md_globals):
        self.add_inline(md, 'vimeo', Vimeo,
            r'([^(])http://(www.|)vimeo\.com/(?P<vimeoid>\d+)\S*')
        self.add_inline(md, 'youtube', Youtube,
            r'([^(])http://www\.youtube\.com/watch\?\S*v=(?P<youtubeargs>[A-Za-z0-9_&=-]+)\S*')
        self.add_inline(md, 'scribd', Scribd,
            r'([^(])http://www\.scribd\.com/doc/(?P<scribdid>\d+)\S*')
        self.add_inline(md, 'image', Image,
            r'([^(]){image}{(?P<imgalign>\S*)}http://(?P<imagelink>\S*)\]')
        self.add_inline(md, 'slide', Slide,
            r'([^(]){slide}(?P<slide>\S*)\]')

class Vimeo(markdown.inlinepatterns.Pattern):
    def handleMatch(self, m):
        url = 'http://vimeo.com/moogaloop.swf?clip_id=%s&amp;server=vimeo.com' % m.group('vimeoid')
        width = self.ext.config['vimeo_width'][0]
        height = self.ext.config['vimeo_height'][0]
        return flash_object(url, width, height)

class Youtube(markdown.inlinepatterns.Pattern):
    def handleMatch(self, m):
        url = 'http://www.youtube.com/v/%s?version=3&theme=light&color=red' % m.group('youtubeargs')
        width = self.ext.config['youtube_width'][0]
        height = self.ext.config['youtube_height'][0]
        return flash_object(url, width, height)

class Scribd(markdown.inlinepatterns.Pattern):
    def handleMatch(self, m):
        url = 'http://www.scribd.com/embeds/%s/content?start_page=1&view_mode=list' % m.group('scribdid')
        width = self.ext.config['scribd_width'][0]
        height = self.ext.config['scribd_height'][0]
        return doc_object(url, width, height)
class Image(markdown.inlinepatterns.Pattern):
    def handleMatch(self, m):
        align = '%s'%m.group('imgalign')
        url = 'http://%s' % m.group('imagelink')
        return img_object(url, align)
class Slide(markdown.inlinepatterns.Pattern):
    def handleMatch(self, m):
        contain = '%s' % m.group('slide')
        return slide_object(contain)
def img_object(url,align):
    align = align.split('|')
    check_image = url[-4:]
    if(check_image == '.png' or check_image ==".gif" or check_image == ".jpg" or check_image =="jpeg"):
        obj = markdown.util.etree.Element('img')
        align_po = 'align-%s'%align[0]
        obj.set('class',align_po)
        obj.set('style','border:1px solid #e3e3e3; padding:6px 6px; border-radius: 2px 2px')
        obj.set('src', url)
        if len(align) == 2:
            size = align[1].split('*')
            if len(size) == 2:
                obj.set('width',size[0])
                obj.set('height',size[1])
            return obj
        if len(align) == 1:
            return obj
    return url


def slide_object(likeslide):
    row = markdown.util.etree.Element('div')
    row.set('class','row-fluid')
    carousel_js = 'carousel_js%s'%random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
    carousel =  markdown.util.etree.Element('div')
    carousel.set('class','carousel slide span9')
    carousel.set('id',carousel_js)
    carousel_inner =  markdown.util.etree.Element('div')
    carousel_inner.set('class','carousel-inner')

    item_1 =  markdown.util.etree.Element('div')
    item_1.set('class','active item')
    one = 1
    contain = likeslide.split('|')
    for url in contain:
        if one == 1:
            thumbnails = markdown.util.etree.Element('div')
            thumbnails.set('clas','thumbnails')
            obj = markdown.util.etree.Element('img')
            obj.set('src', url)
            obj.set('width', '100%')
            thumbnails.append(obj)
            item_1.append(thumbnails)
            carousel_inner.append(item_1)
            one = 2
        else:
            thumbnails = markdown.util.etree.Element('div')
            thumbnails.set('clas','thumbnails')
            item_n =  markdown.util.etree.Element('div')
            item_n.set('class','item')
            obj = markdown.util.etree.Element('img')
            obj.set('src', url)
            obj.set('width', '100%')
            thumbnails.append(obj)
            item_n.append(thumbnails)
            carousel_inner.append(item_n)
    carousel.append(carousel_inner)
    carousel_js_like = '#' + carousel_js 
    prev = markdown.util.etree.Element('a')
    prev.set('href', carousel_js_like)
    prev.set('class','carousel-control left')
    prev.set('data-slide','prev')
    prev.text = "&lsaquo;"

    next = markdown.util.etree.Element('a')
    next.set('href', carousel_js_like)
    next.set('class','carousel-control right')
    next.set('data-slide','next')
    next.text = "&rsaquo;"

    carousel.append(prev)
    carousel.append(next)
    row.append(carousel)
    return row

def doc_object(url, width, height):
    obj = markdown.util.etree.Element('iframe')
    obj.set('src', url)
    obj.set('width', width)
    obj.set('height', height)
    obj.set('data-auto-height', 'true')
    obj.set('data-aspect-ratio', '0.772727272727273')
    obj.set('scrolling', 'yes')
    obj.set('frameborder', '0')
    return obj

def flash_object(url, width, height):
    obj = markdown.util.etree.Element('iframe')
    obj.set('width', width)
    obj.set('height', height)
    obj.set('src', url)
    obj.set('type', 'text/html')
    obj.set('frameborder','0')
    obj.set('name', 'allowFullScreen')
    return obj
def makeExtension(configs=None) :
    return VideoExtension(configs=configs)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
