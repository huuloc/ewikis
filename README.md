## <http://wikis.herokuapp.com/>

## Software Engineering @ UET, Spring 2012

* Lecturer: Truong Anh Hoang
* Group 5:  Engrade Wikis
* Members:
  1. Vu Xuan Lai
  2. Dang Van Linh
  3. Vu Thai Linh
  4. To Ngoc Linh
  5. Bui Huu Loc

## Installing components:

* `pip install -r requirements.txt`

## Installing custom markdown:

copy folder `extension` to over write `extensions` in `site-packages/markdown`